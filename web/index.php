<?php

require_once('../YandexDirectAPI.php');
require_once('../storageSQL.php');


$apiClient = new YandexDirectAPI("fodev", "cfb72cb84e2940bf80a8eafefc0defc1");


$directCampaigns = $apiClient->getCampaignList();

//print_r($directCampaigns);
$storage = new StatsStorage("configsql.json");

$localCampaign = $storage->getCampaigns();


$storage->insertCampaigns($directCampaigns);

//print_r($storage->getCampaignIDs());
$localCampaignIDs = $storage->getCampaignIDs();
$directbanners = $apiClient->GetBanners($localCampaignIDs);
$storage->insertBanners($directbanners);
//print_r($apiClient->GetBanners(array("69500")));
//print_r(array('BannerIDS' => $storage->getCampaignIDs()));


try {
    $startDate = new DateTime($_GET['date1']); //2014-10-01
    $endDate = new DateTime($_GET['date2']); //2014-10-07
} catch (\Exception $e) {
    $this->connection->rollBack();
    throw new \Exception("Ошибка времени", 0, $e);
}

$id_Campaign = $_GET['id_campaign'];


if ($startDate == null || $endDate == null) {
    $startDate = new DateTime('now');
    $endDate = new DateTime('now');
}


if ($id_Campaign === null) {
    foreach ($localCampaignIDs as $localCampaignID) {

//echo $localCampaignID."<br>";

        $BannersStat = $apiClient->GetBannersStat($localCampaignID, $startDate, $endDate);
        $storage->insertBannersStat($BannersStat);

    }
} else {
    $BannersStat = $apiClient->GetBannersStat($id_Campaign, $startDate, $endDate);
    $storage->insertBannersStat($BannersStat);
}






