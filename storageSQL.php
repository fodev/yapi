<?php

class StatsStorage
{

    /** @var  string */
    private $host;

    /** @var  string */
    private $dbname;

    /** @var  string */
    private $user;

    /** @var  string */
    private $pass;

    /** @var  \PDO */
    private $connection;


    /**
     * @param string $sqlconf
     * @throws Exception
     */
    function __construct($sqlconf)
    {
        if (!file_exists($sqlconf)) {
            throw new \Exception("нет файла");
        }

        $config = json_decode(file_get_contents($sqlconf), true);

        $this->host = $config['host'];
        $this->dbname = $config['dbname'];
        $this->user = $config['user'];
        $this->pass = $config['pass'];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCampaigns()
    {
        $connection = $this->getConnection();

        try {
            $sth = $connection->query('SELECT id, campaign_name FROM campaign');
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception('Ошибка запроса к БД', 0, $e);
        }

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return PDO
     * @throws \Exception
     */
    private function getConnection()
    {

        if ($this->connection === null) {
            try {

                $this->connection = new PDO(
                    'mysql:host=' . $this->host . ';dbname=' . $this->dbname,
                    $this->user,
                    $this->pass
                );

                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (PDOException $e) {
                throw new \Exception('Ошика подключени к БД: ' . $e->getMessage());
            }
        }

        return $this->connection;
    }

    /**
     * @param array $BannersStat
     * @throws \Exception
     */

    public function insertBannersStat(array $BannersStat)
    {

        if (!empty($BannersStat)) {
            return 0;
        }
        $connection = $this->getConnection();


        try {

            $connection->beginTransaction();
            $sth = $connection->prepare("DELETE FROM stat WHERE date BETWEEN ? AND ?");
            $sth->execute(array($BannersStat['StartDate'], $BannersStat['EndDate']));

            foreach ($BannersStat['Stat'] as $Stat) {

                $sth = $connection->prepare(
                    "INSERT INTO stat ( banner_id, campaign_id , date, clicks , sum ) values (?, ?, ?, ?, ?)"
                );

                $sth->execute(array(
                    $Stat['BannerID'],
                    $BannersStat['CampaignID'],
                    $Stat['StatDate'],
                    $Stat['Clicks'],
                    $Stat['Sum']
                ));


            }


            $connection->commit();


        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception("Ошибка обновления кампаний", 0, $e);
        }
    }

    /**
     * @param array $bannerlist
     * @throws \Exception
     */
    public function insertBanners(array $bannerlist)
    {
        $connection = $this->getConnection();
        $localBannerIDs = $this->getBannerIDs();


        try {
            $chunked_banners = array_chunk($bannerlist, 100);
            foreach ($chunked_banners as $banners) {
                $connection->beginTransaction();

                foreach ($banners as $banner) {

                    if (in_array($banner['BannerID'], $localBannerIDs)) {

                        $sth = $connection->prepare(
                            "UPDATE banner SET banner_name = ?, campaign_id=? WHERE id = ?"
                        );

                        $sth->execute(array(
                            $banner['Text'],
                            $banner['CampaignID'],
                            $banner['BannerID']


                        ));

                    } else {
                        $sth = $connection->prepare(
                            "INSERT INTO banner (id, campaign_id, banner_name ) values (?, ?, ?)"
                        );

                        $sth->execute(array(
                            $banner['BannerID'],
                            $banner['CampaignID'],
                            $banner['Text']
                        ));
                    }

                }

                /* Фиксация изменений */
                $connection->commit();
            }

        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception("Ошибка обновления кампаний", 0, $e);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getBannerIDs()
    {
        $connection = $this->getConnection();

        try {
            $sth = $connection->query('SELECT id FROM banner');
            while ($row = $sth->fetch()) {
                $ids[] = $row['id'];
            }
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception('Ошибка запроса к БД', 0, $e);
        }


        return $ids;
    }

    /**
     * @param array $campaignlist
     * @throws \Exception
     */
    public function insertCampaigns(array $campaignlist)
    {
        $connection = $this->getConnection();


        $localCampaignIDs = $this->getCampaignIDs();


        try {
            $chunked_campaigns = array_chunk($campaignlist, 100);
            foreach ($chunked_campaigns as $campaigns) {
                $connection->beginTransaction();

                foreach ($campaigns as $campaign) {

                    if (in_array($campaign['CampaignID'], $localCampaignIDs)) {

                        $sth = $connection->prepare(
                            "UPDATE campaign SET campaign_name = ? , status_archive = ? , is_active=? , status_show=? WHERE id = ?"
                        );
                        if ($campaign['IsActive'] === 'Yes') $campaign['IsActive'] = true; else {
                            false;
                        }
                        if ($campaign['StatusShow'] === 'Yes') $campaign['StatusShow'] = true; else {
                            false;
                        }
                        if ($campaign['StatusArchive'] === 'Yes') $campaign['StatusArchive'] = true; else {
                            false;
                        }
                        $sth->execute(array(
                            $campaign['Name'],
                            $campaign['StatusArchive'],
                            $campaign['IsActive'],
                            $campaign['StatusShow'],
                            $campaign['CampaignID']
                        ));

                    } else {
                        $sth = $connection->prepare(
                            "INSERT INTO campaign (id, campaign_name , status_archive , is_active , status_show ) values (?, ?, ?, ?, ?)"
                        );

                        $sth->execute(array(
                            $campaign['CampaignID'],
                            $campaign['Name'],
                            $campaign['StatusArchive'],
                            $campaign['IsActive'],
                            $campaign['StatusShow']
                        ));
                    }

                }

                /* Фиксация изменений */
                $connection->commit();
            }

        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception("Ошибка обновления кампаний", 0, $e);
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCampaignIDs()
    {
        $connection = $this->getConnection();

        try {
            $sth = $connection->query('SELECT id FROM campaign');
            while ($row = $sth->fetch()) {
                $ids[] = $row['id'];
            }
        } catch (\Exception $e) {
            $this->connection->rollBack();
            throw new \Exception('Ошибка запроса к БД', 0, $e);
        }


        return $ids;
    }

}