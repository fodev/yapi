<?php

class YandexDirectAPI
{
# логин в Директе
    private $login;
    private $token;

    private $sum_limit;
    private $click_limit;


    /**
     * @param string $login
     * @param string $token
     */


    function __construct($login, $token)
    {
        $this->login = $login;
        $this->token = $token;
        $this->sumLimit = 200;
        $this->clickLimit = 7;
    }

    public function changeLimits($sumLimit, $clickLimit)
    {
        $this->sumLimit = $sumLimit;
        $this->clickLimit = $clickLimit;

    }

    public function getCampaignList()
    {
        return $this->sendRequest(array(
            'method' => 'GetCampaignsList',

            'param' => array($this->login)

        ));
    }

    private function sendRequest($request)
    {

        $request['locale'] = 'ru';
        $request['token'] = $this->token;
# преобразование в JSON-формат
        $request = json_encode($request);

# параметры запроса
        $opts = array(
            'http' => array(
                'method' => "POST",
                'content' => $request,
            )
        );

        $context = stream_context_create($opts);

        try {
            $response = file_get_contents('https://api-sandbox.direct.yandex.ru/live/v4/json/', 0, $context);
        } catch (PDOException $e) {
            throw new \Exception('Ошика подключени к API: ' . $e->getMessage());
        }
        $result = json_decode($response, true);

//     echo $response;
        if (isset($result['error_detail'])) return array();
        if (empty($result) || !isset($result['data'])) {
            throw new \Exception('Ошибка выполнения запроса');
        }

        return $result['data'];
    }

    public function GetBanners($CampaignIDs)
    {
        return $this->sendRequest(array(
            'method' => 'GetBanners',
            'param' => array('CampaignIDS' => $CampaignIDs)

        ));

    }

    public function GetBannersStat($CampaignID, DateTime $startDate, DateTime $endDate)
    {

        $interval = $startDate->diff($endDate);
        if ($startDate > new DateTime('now') || $endDate > new DateTime('now')) Throw new \Exception("указана дата из будущего");
        if ($startDate > $endDate || $interval->format('%d') > 7) Throw new \Exception("Ошибка интервала времени");

        $Stats = $this->sendRequest(array(
            'method' => 'GetBannersStat',
            'param' => array(
                'CampaignID' => $CampaignID, 'StartDate' => $startDate->format('Y-m-d'),
                'EndDate' => $endDate->format('Y-m-d'), 'GroupByColumns' => array('clDate', 'clPhrase', 'clBanner'))

        ));
        if (empty($Stats)) return array();
        foreach ($Stats['Stat'] as $Stat) {
            if (($Stat['Clicks'] < $this->clickLimit || $Stat['Sum'] > $this->sumLimit) && $Stat['StatDate'] == date('Y-m-d', strtotime("yesterday"))) $this->StopCampaign($CampaignID);
        }

        return $Stats;


    }

    public function StopCampaign($CampaignID)
    {

//echo "5";
        return $this->sendRequest(array(
            'method' => 'StopCampaign',
            'param' => array('CampaignID' => $CampaignID)
        ));


    }

    private function utf8($struct)
    {
        foreach ($struct as $key => $value) {
            if (is_array($value)) {
                $struct[$key] = $this->utf8($value);
            } elseif (is_string($value)) {
                $struct[$key] = utf8_encode($value);
            }
        }

        return $struct;
    }
}
